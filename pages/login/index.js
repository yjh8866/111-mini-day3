Page({
  data: {
    // 是否倒计时
    countDownVisible: false,
    // 倒计时起始值
    time: 10,
  },

  onLoad() {
    const pageStack = getCurrentPages();
    console.log(pageStack);
  },

  // 1. 获取短信验证码
  getSMSCode() {
    // ...
    this.setData({
      countDownVisible: true,
    });
  },

  // 2. 倒计时业务处理
  setCountDown(ev) {
    // console.log(ev);
    this.setData({
      time: ev.detail,
    });
    // 判断
    if (ev.detail === 0) {
      this.setData({ countDownVisible: false, time: 10 });
    }
  },
});
