Component({
  options: {
    // 允许子组件使用父级的样式类
    addGlobalClass: true,
    // 开启组件支持多插槽
    multipleSlots: true,
  },
  // custom-class 和  custom-title 组件提供给外部传递样式类名的属性
  externalClasses: ["custom-class", "custom-title"],

  data: {
    message: "我是打酱油的",
    statusBarHeight: 0,
  },

  // -------------自定义属性-begin-----------------
  properties: {
    back: Boolean, // 控制返回箭头的显示/隐藏
    // 控制回退的步数
    delta: {
      type: Number,
      value: 1,
    },
  },
  // -----------------自定义属性-end---------------

  // ----------------生命周期-begin-----------------
  lifetimes: {
    created() {
      // console.log("created-组件创建时触发, 不能this.setData");
      // console.log(this.data);
      // this.setData({
      //   message: "我打完了.....",
      // });
      // 一般用于直接给this添加属性, 比如: 版本, 作者
      // this.version = "v1.0.0";
      // this.author = "刘德华";
      // console.log(this);
    },
    attached() {
      // console.log("attached-组件初始化完毕后触发, 可以做大部分业务");
      // 场景: 更新数据
      // this.setData({
      //   message: "我打完了.....",
      // });

      // 同步获取系统信息
      const sys = wx.getSystemInfoSync();
      // console.log(sys.statusBarHeight);
      this.setData({
        statusBarHeight: sys.statusBarHeight,
      });
    },
  },
  // ----------------生命周期-end-----------------

  // ----------------组件函数-begin-----------------
  methods: {
    onBack() {
      // console.log("触发了...");
      wx.navigateBack({
        delta: this.data.delta,
      });
    },
  },
  // ----------------组件函数-end-----------------
});
