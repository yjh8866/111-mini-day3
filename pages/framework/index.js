// 获取全局的应用实例
const app = getApp();
Page({
  data: {
    nickname: wx.getStorageSync("nickname"),
    avatar: wx.getStorageSync("avatar"),
    message: app.message,
  },

  onLoad() {
    // console.log(app);
  },

  onShow() {
    console.log("onShow被执行了-8888888");
    this.setData({
      nickname: wx.getStorageSync("nickname"),
      avatar: wx.getStorageSync("avatar"),
    });

    // 获取当前的页面栈(数组)
    const pageStack = getCurrentPages();
    console.log(pageStack);
  },

  changeGlobalMessage() {
    app.changeMessage();
    this.setData({
      message: app.message,
    });
  },
});
