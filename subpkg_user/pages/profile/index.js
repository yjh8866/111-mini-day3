const app = getApp();
Page({
  data: {
    avatar: "",
  },
  onLoad() {
    // 1. 获取当前的页面栈(数组)
    const pageStack = getCurrentPages();
    console.log(pageStack);
    // 2. 常用操作:
    // 从页面栈中拿到其中一个页面对象, 去操作其内部的属性/函数/生命周期方法
    const pageInstance = pageStack[0];
    //----- 2.1 获取页面对象中data中的数据
    // console.log(pageInstance.data.message);
    //----- 2.2 修改页面对象中data中的数据
    // pageInstance.setData({
    //   message: "6666666.....",
    // });
    //----- 2.3 重新页面对象中的生命周期钩子函数
    // pageInstance.onShow = () => {
    //   console.log("页面的onShow被改了");
    // };
    //----- 2.4 获取页面对象对应的路径
    // console.log(pageInstance.route);

    // ------3.判断是否已经登录
    const isLogin = !!app.token;
    if (!isLogin) {
      // 没有登录, 跳转到登录页面中
      // 特点: 保留当前页, 正常跳转, 把下一个页面放入页面栈
      // wx.navigateTo({
      //   url: "/pages/login/index",
      // });

      // 特点: 关闭当前页, 再跳转到新页面
      wx.redirectTo({
        url: "/pages/login/index?returnUrl=/" + pageInstance.route,
      });
    }
  },
  // 1. 获取用户的昵称并存储到本地
  getUserNickName(ev) {
    //  console.log(ev);
    wx.setStorageSync("nickname", ev.detail.value);
  },
  // 2. 获取用户头像并存储到本地
  getUserAvatar(ev) {
    this.setData({
      avatar: ev.detail.avatarUrl,
    });
    wx.setStorageSync("avatar", ev.detail.avatarUrl);
  },
});
