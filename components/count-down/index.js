Component({
  externalClasses: ["custom-class"],
  properties: {
    // 接收外部传入的倒计时起始值
    time: {
      type: Number,
      value: 60,
    },
    useSlot: Boolean, // 是否使用插槽
  },
  lifetimes: {
    attached() {
      this.countStart();
    },
  },
  methods: {
    countStart() {
      let timer = null;
      // 停止定时器
      if (this.data.time === 0) return clearTimeout(timer);
      // 更新渲染
      this.setData({
        time: (this.data.time -= 1),
      });
      // 定时器
      timer = setTimeout(this.countStart.bind(this), 1000);
      // 传递数据到组件外部
      console.log(this.data.time);
      this.triggerEvent("change", this.data.time);
    },
  },
});
