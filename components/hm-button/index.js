Component({
  properties: {
    money: Number,
  },
  methods: {
    getMoney() {
      this.triggerEvent("changeMoney", this.data.money + 20000);
    },
  },
});
